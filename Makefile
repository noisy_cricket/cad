.PHONY: stl dxf

stl:
	openscad --export-format binstl -o upper_case.stl upper_case.scad
	openscad --export-format binstl -o lower_case.stl lower_case.scad
	openscad --export-format binstl -o plate.stl plate.scad

dxf:
	sed -i 's/\/\/ projection/projection/' plate.scad; openscad --export-format dxf -o plate.dxf plate.scad; sed -i 's/projection/\/\/ projection/' plate.scad
	openscad --export-format dxf -o weight.dxf weight.scad

