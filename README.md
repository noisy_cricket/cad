![upper case](https://gitlab.com/noisy_cricket/cad/-/raw/main/upper_case.png?inline=false)
![plate](https://gitlab.com/noisy_cricket/cad/-/raw/main/plate.png?inline=false)
![lower case](https://gitlab.com/noisy_cricket/cad/-/raw/main/lower_case.png?inline=false)

# CAD files for noisy cricket

Create files for 3D printing or CNC by running this (needs openscad):

```shell
make stl
make 3mf
make dxf
```

_You only need the DXF files if you want to CNC the plate or weight. I also included STEP files for CNCing._

## Hardware:

- Threaded inserts for the plate, USB C daughter board, standoffs, weight and upper case are M2, height 2mm, outer diameter 3.2 (18x)
- Screws for the plate and USB-C daughter board are M2, length 3-3.5mm (6x)
- Screws for the PCB mounting are M2, length 3.5-4.6mm (4x)
- Screws to combine upper and lower case are M2, length 15.5-16.6 (4x)
- Standoffs are M2, length 3mm (4x)
- Screws for the weight are counter sunk M2, length 3-3.5mm (4x)

Threaded inserts from [AliExpress](https://www.aliexpress.com/item/1005004873154281.html).
Screws from [AliExpress](https://www.aliexpress.com/item/32810852732.html).
Screws for the weight from [AliExpress](https://www.aliexpress.com/item/1005002044837898.html).
Standoffs from [AliExpress](https://www.aliexpress.com/item/1005003328519584.html).

You can also find the CAD files on [printables.com](https://www.printables.com/model/531838-noisy-cricket-mini-gaming-keyboard).

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Made with ❤️ by XenGi.
