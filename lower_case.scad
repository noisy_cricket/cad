// This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

include <util.scad>

$fn = 64;
with_weight = true;

color("SaddleBrown") difference() {
  // case
  union() {
    hull() {
      translate([-case_thickness, -case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+0.5);
      translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+0.5);
      translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+0.5);
      translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+0.5);
    }
    
    translate([0,0,0]) hull() {
      translate([-case_thickness, -case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r1=pcb_radius+0.5, r2=pcb_radius+2);
      translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r1=pcb_radius+0.5, r2=pcb_radius+2);
      translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r1=pcb_radius+0.5, r2=pcb_radius+2);
      translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r1=pcb_radius+0.5, r2=pcb_radius+2);
    }
  }

  // pcb
  translate([0, 0, case_bottom_thickness]) hull() {
    cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+1);
    translate([pcb_width-2*pcb_radius, 0, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+1);
    translate([0, pcb_depth-2*pcb_radius, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+1);
    translate([pcb_width-2*pcb_radius, pcb_depth-2*pcb_radius, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, r=pcb_radius+1);
  }

  // plate_mounting
  translate([20, pcb_depth - pcb_radius+1, case_thickness+standoff_height+pcb_thickness+5-plate_thickness]) lug(hole=false);
  translate([pcb_width-2*pcb_radius - 20, pcb_depth - pcb_radius+1, case_thickness+standoff_height+pcb_thickness+5-plate_thickness]) lug(hole=false);
  translate([20, -pcb_radius-1, case_thickness+standoff_height+pcb_thickness+5-plate_thickness]) lug(north_facing=false, hole=false);
  translate([pcb_width-2*pcb_radius - 20, -pcb_radius-1, case_thickness+standoff_height+pcb_thickness+5-plate_thickness]) lug(north_facing=false, hole=false);
  // screw holes
  translate([20, pcb_depth - pcb_radius+3, case_thickness+standoff_height+pcb_thickness+5-plate_thickness-threaded_insert_height]) cylinder(h=threaded_insert_height, d=3);  // top-left
  translate([pcb_width-2*pcb_radius - 20, pcb_depth - pcb_radius+3, case_thickness+standoff_height+pcb_thickness+5-plate_thickness-threaded_insert_height]) cylinder(h=threaded_insert_height, d=3);  // top-right
  translate([20, -pcb_radius-3, case_thickness+standoff_height+pcb_thickness+5-plate_thickness-threaded_insert_height]) cylinder(h=threaded_insert_height, d=3);  // bottom-left
  translate([pcb_width-2*pcb_radius - 20, -pcb_radius-3, case_thickness+standoff_height+pcb_thickness+5-plate_thickness-threaded_insert_height]) cylinder(h=threaded_insert_height, d=3);  // bottom-right
  
  // USB-C daughter board
  translate([pcb_width/2-pcb_radius, pcb_depth-pcb_radius+1-pololu_depth/2, case_bottom_thickness - pcb_thickness/2]) {
    translate([0,pololu_depth/2-case_thickness+5,0.8+3/2]) rotate([-90,0,0]) hull() {
      translate([-7/2,0,0]) cylinder(h=case_thickness+2, d=8);
      translate([7/2,0,0]) cylinder(h=case_thickness+2, d=8);
    }
    cube([pololu_width, pololu_depth, pcb_thickness+0.1], center=true);
    // screw holes
    translate([-pololu_width/2+pololu_hole_spacing, pololu_depth/2-pololu_hole_spacing, -pcb_thickness/2-threaded_insert_height]) cylinder(h=threaded_insert_height, d=3);
    translate([pololu_width/2-pololu_hole_spacing, pololu_depth/2-pololu_hole_spacing, -pcb_thickness/2-threaded_insert_height]) cylinder(h=threaded_insert_height, d=3);
  }
  
  // cable channel
  translate([65, 26, case_bottom_thickness/2]) {
    translate([-7.5,30,0]) linear_extrude(case_bottom_thickness/2+1) polygon([[7.5,0], [7.5,10.75], [0,10.75]]);
    cube([10, 40.75, case_bottom_thickness/2+1]);
    cube([72, 11, case_bottom_thickness/2+1]);
  }
  
  // screw holes for upper case
  translate([-case_thickness, -case_thickness, 0]) cylinder(h=2, d=4);
  translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) cylinder(h=2, d=4);
  translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=2, d=4);
  translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=2, d=4);
  
  translate([-case_thickness, -case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, d=2);
  translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, d=2);
  translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, d=2);
  translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=case_thickness+standoff_height+pcb_thickness+5, d=2);

  // mounting standoffs
  for(i = [0 : len(mounting_holes)-1]) {
    translate([mounting_holes[i][0] - pcb_radius, mounting_holes[i][1] - pcb_radius, case_bottom_thickness - 3]) union() {
      translate([0,0,1]) cylinder(h=2, d=3);
      cylinder(h=3, d=2);
    }
  }

  // hole for reset button
  translate([reset_x - pcb_radius, reset_y - pcb_radius, 0]) cylinder(h=case_thickness, r=1);

  // OPTIONAL: hole for weight
  if (with_weight) {
    hull() {
      translate([5,5,0]) cylinder(h=1.5, d=10);
      translate([pcb_width-2*pcb_radius-5,5,0]) cylinder(h=1.5, d=10);
      translate([5,pcb_depth-2*pcb_radius-5,0]) cylinder(h=1.5, d=10);
      translate([pcb_width-2*pcb_radius-5,pcb_depth-2*pcb_radius-5,0]) cylinder(h=1.5, d=10);
    }
    translate([pcb_width/2,pcb_depth/2+6,1.5]) mirror([1,0,0]) linear_extrude(0.2) text(text="noisy cricket", size=10, font="Liberation Sans:style=Bold", halign="center");
    translate([pcb_width/2,pcb_depth/2-8,1.5]) mirror([1,0,0]) linear_extrude(0.2) text(text="© XenGi 2023", size=6, font="Liberation Sans:style=Bold", halign="center");
    // threaded inserts
    translate([10,10,1.5]) cylinder(h=threaded_insert_height, d=3);
    translate([pcb_width-2*pcb_radius-10,10,1.5]) cylinder(h=threaded_insert_height, d=3);
    translate([10,pcb_depth-2*pcb_radius-10,1.5]) cylinder(h=threaded_insert_height, d=3);
    translate([pcb_width-2*pcb_radius-10,pcb_depth-2*pcb_radius-10,1.5]) cylinder(h=threaded_insert_height, d=3);
    //reset button text
    translate([reset_x - pcb_radius-1.4, reset_y - pcb_radius-1.5, 1.5]) mirror([1,0,0]) rotate([0,0,90]) linear_extrude(0.2) text(text="RESET ►", size=3, font="Liberation Sans:style=Bold", halign="right");
  } else {
    translate([pcb_width/2,pcb_depth/2+6,0]) mirror([1,0,0]) linear_extrude(0.2) text(text="noisy cricket", size=10, font="Liberation Sans:style=Bold", halign="center");
    translate([pcb_width/2,pcb_depth/2-8,0]) mirror([1,0,0]) linear_extrude(0.2) text(text="© XenGi 2023", size=6, font="Liberation Sans:style=Bold", halign="center");
    translate([reset_x - pcb_radius-1.4, reset_y - pcb_radius-1.5, 0]) mirror([1,0,0]) rotate([0,0,90]) linear_extrude(0.2) text(text="RESET ►", size=3, font="Liberation Sans:style=Bold", halign="right");
  }
}