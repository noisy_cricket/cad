// This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

include <util.scad>

$fn = 64;

// projection(cut=true)
color("Gold") difference() {
  union() {
    hull() {
      cylinder(h=plate_thickness, r=pcb_radius+1);
      translate([pcb_width-2*pcb_radius, 0, 0]) cylinder(h=plate_thickness, r=pcb_radius+1);
      translate([0, pcb_depth-2*pcb_radius, 0]) cylinder(h=plate_thickness, r=pcb_radius+1);
      translate([pcb_width-2*pcb_radius, pcb_depth-2*pcb_radius, 0]) cylinder(h=plate_thickness, r=pcb_radius+1);
    }

    translate([20, pcb_depth - pcb_radius+1, 0]) lug();
    translate([pcb_width-2*pcb_radius - 20, pcb_depth - pcb_radius+1, 0]) lug();
    translate([20, -pcb_radius-1, 0]) lug(north_facing=false);
    translate([pcb_width-2*pcb_radius - 20, -pcb_radius-1, 0]) lug(north_facing=false);
  }

  translate([12.7-pcb_radius, 12.7-pcb_radius, 0]) {
    mx_1x1_cutout();
    translate([mx, 0, 0]) mx_1x1_cutout();
    translate([2*mx, 0, 0]) mx_1x1_cutout();
    translate([3*mx, 0, 0]) mx_1x1_cutout();
    translate([4.5*mx, 0, 0]) mx_2x1_cutout();
    translate([4.5*mx, 0, 0]) mx_1x1_cutout();

    translate([(mx*1.75)/2-mx/2, mx, 0]) mx_1x1_cutout();
    translate([mx*1.75-mx, mx, 0]) {
      translate([mx, 0, 0]) mx_1x1_cutout();
      translate([2*mx, 0, 0]) mx_1x1_cutout();
      translate([3*mx, 0, 0]) mx_1x1_cutout();
      translate([4*mx, 0, 0]) mx_1x1_cutout();
      translate([4*mx+21.4312, 9.525, 0]) mx_1x2_cutout();
    };

    translate([(mx*1.5)/2-mx/2, 2*mx, 0]) mx_1x1_cutout();
    translate([(mx*1.5)-mx, 2*mx, 0]) {
      translate([mx, 0, 0]) mx_1x1_cutout();
      translate([2*mx, 0, 0]) mx_1x1_cutout();
      translate([3*mx, 0, 0]) mx_1x1_cutout();
      translate([4*mx, 0, 0]) mx_1x1_cutout();
    };

    translate([0, 3*mx, 0]) {
      mx_1x1_cutout();
      translate([mx, 0, 0]) mx_1x1_cutout();
      translate([2*mx, 0, 0]) mx_1x1_cutout();
      translate([3*mx, 0, 0]) mx_1x1_cutout();
      translate([4*mx, 0, 0]) mx_1x1_cutout();
      translate([5*mx, 0, 0]) mx_1x1_cutout();
      translate([6*mx, 0, 0]) mx_1x1_cutout();
    };
  };
  // potentiometer
  translate([127-pcb_radius, 12-pcb_radius, plate_thickness/2]) cube([13, 14, plate_thickness], center=true);
};

