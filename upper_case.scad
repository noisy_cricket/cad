// This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

include <util.scad>

$fn = 64;

color("SaddleBrown") difference() {
  // case
  union() {
    hull() {
      translate([-case_thickness, -case_thickness, 0]) cylinder(h=upper_case_height/2, r=pcb_radius+2);
      translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) cylinder(h=upper_case_height/2, r=pcb_radius+2);
      translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=upper_case_height/2, r=pcb_radius+2);
      translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=upper_case_height/2, r=pcb_radius+2);
    }
    translate([0,0,upper_case_height/2]) hull() {
      translate([-case_thickness, -case_thickness, 0]) resize(newsize=[2*(pcb_radius+2),2*(pcb_radius+2),upper_case_height]) sphere(r=pcb_radius+2);
      translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) resize(newsize=[2*(pcb_radius+2),2*(pcb_radius+2),upper_case_height]) sphere(r=pcb_radius+2);
      translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) resize(newsize=[2*(pcb_radius+2),2*(pcb_radius+2),upper_case_height]) sphere(r=pcb_radius+2);
      translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) resize(newsize=[2*(pcb_radius+2),2*(pcb_radius+2),upper_case_height]) sphere(r=pcb_radius+2);
    }
  }

  // pcb
  hull() {
    translate([3,3,0]) cylinder(h=upper_case_height+0.1, r=pcb_radius+1);
    translate([pcb_width-2*pcb_radius-3, 3, 0]) cylinder(h=upper_case_height+0.1, r=pcb_radius+1);
    translate([3, pcb_depth-2*pcb_radius-3, 0]) cylinder(h=upper_case_height+0.1, r=pcb_radius+1);
    translate([pcb_width-2*pcb_radius-3, pcb_depth-2*pcb_radius-3, 0]) cylinder(h=upper_case_height+0.1, r=pcb_radius+1);
  }
  
  // threaded inserts
  translate([-case_thickness, -case_thickness, 0]) cylinder(h=threaded_insert_height, d=3);
  translate([pcb_width-2*pcb_radius+case_thickness, -case_thickness, 0]) cylinder(h=threaded_insert_height, d=3);
  translate([-case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=threaded_insert_height, d=3);
  translate([pcb_width-2*pcb_radius+case_thickness, pcb_depth-2*pcb_radius+case_thickness, 0]) cylinder(h=threaded_insert_height, d=3);
  
  // space for screws from lower case
  translate([20, pcb_depth - pcb_radius+1+case_thickness/2, 0]) cylinder(h=2.3, d=4.2);
  translate([pcb_width-2*pcb_radius - 20, pcb_depth - pcb_radius+1+case_thickness/2, 0]) cylinder(h=2.3, d=4.2);
  translate([20, -pcb_radius-1-case_thickness/2, 0]) cylinder(h=2.3, d=4.2);
  translate([pcb_width-2*pcb_radius - 20, -pcb_radius-1-case_thickness/2, 0]) cylinder(h=2.3, d=4.2);
}
