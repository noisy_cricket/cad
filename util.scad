// This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

$fn=64;

pcb_width = 139.7;
pcb_depth = 82.55;
pcb_radius = 3.175;
pcb_thickness = 1.6;

plate_thickness = 1.5;
threaded_insert_height = 2.5;

case_thickness = 5;
case_bottom_thickness = 5;  // TODO: doesn't respect height of PCB plus switches etc
case_height = 10;
upper_case_height = 5;

standoff_height = 3;

reset_x = 111.1;
reset_y = 47.6;

mx = 19.05;

mounting_holes = [
  [36.5125, 23.0188],
  [30.9563, 57.9438],
  [116.6813, 14.2875],
  [98.425, 69.0563]
];

pololu_width = 0.8*2.54*10+0.2;
pololu_depth = 0.5*2.54*10+1;
pololu_holes_radius = 1;
pololu_hole_spacing = 0.1*2.54*10;


module lug(north_facing=true, hole=true) {
  difference() {
    hull() {
      translate([0, north_facing ? 2 : -2, 0]) cylinder(h=plate_thickness, d=4);
      translate([-5, 0, 0]) cylinder(h=plate_thickness, d=2);
      translate([5, 0, 0]) cylinder(h=plate_thickness, d=2);
    }
    if (hole) {
      translate([0, north_facing ? 2 : -2, 0]) cylinder(h=plate_thickness, d=2.1);  // M2
    }
  }
}

module mx_1x1_cutout() {
  side_length = 14;

  // central hole
  translate([0, 0, pcb_thickness/2]) cube([side_length, side_length, pcb_thickness+0.1], center=true);

  // wings
  translate([side_length/2, 2.5+3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([side_length/2, -2.5-3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([-side_length/2, 2.5+3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([-side_length/2, -2.5-3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);

  // rounded corners
  translate([side_length/2-0.25, side_length/2-0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([side_length/2-0.25, -side_length/2+0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([-side_length/2+0.25, side_length/2-0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([-side_length/2+0.25, -side_length/2+0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);

}

module mx_1x2_cutout() {
  side_length = 14;

  // central hole
  translate([0, 0, pcb_thickness/2]) cube([side_length, side_length+2, pcb_thickness+0.1], center=true);

  // wings
  translate([side_length/2, 2.5+3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([side_length/2, -2.5-3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([-side_length/2, 2.5+3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([-side_length/2, -2.5-3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);

  // rounded corners
  translate([side_length/2-0.25, side_length/2-0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([side_length/2-0.25, -side_length/2+0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([-side_length/2+0.25, side_length/2-0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([-side_length/2+0.25, -side_length/2+0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);

  // stabilizers
  translate([0, 0, pcb_thickness/2]) {
    translate([-0.5, 11.9, 0]) cube([15, 8, pcb_thickness+0.1], center=true);
    translate([-0.5, -11.9, 0]) cube([15, 8, pcb_thickness+0.1], center=true);
  }
}

module mx_2x1_cutout() {
  side_length = 14;

  // central hole
  translate([0, 0, pcb_thickness/2]) cube([side_length+2, side_length, pcb_thickness+0.1], center=true);

  // wings
  translate([side_length/2, 2.5+3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([side_length/2, -2.5-3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([-side_length/2, 2.5+3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);
  translate([-side_length/2, -2.5-3.5/2, pcb_thickness/2]) cube([2*0.8, 3.5, pcb_thickness+0.1], center=true);

  // rounded corners
  translate([side_length/2-0.25, side_length/2-0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([side_length/2-0.25, -side_length/2+0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([-side_length/2+0.25, side_length/2-0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);
  translate([-side_length/2+0.25, -side_length/2+0.25, 0]) cylinder(h=pcb_thickness+0.1, r=0.5);

  // stabilizer holes
  translate([0, 0, pcb_thickness/2]) {
    translate([11.9, -0.5, 0]) cube([8, 15, pcb_thickness+0.1], center=true);
    translate([-11.9, -0.5, 0]) cube([8, 15, pcb_thickness+0.1], center=true);
  }
}
