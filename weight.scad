// This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

include <util.scad>

$fn = 64;

projection(cut=true) difference() {
  hull() {
    translate([5,5,0]) cylinder(h=1.5, d=10);
    translate([pcb_width-2*pcb_radius-5,5,0]) cylinder(h=1.5, d=10);
    translate([5,pcb_depth-2*pcb_radius-5,0]) cylinder(h=1.5, d=10);
    translate([pcb_width-2*pcb_radius-5,pcb_depth-2*pcb_radius-5,0]) cylinder(h=1.5, d=10);
  }
  translate([10,10,0]) cylinder(h=1.5, d=2.2);
  translate([pcb_width-2*pcb_radius-10,10,0]) cylinder(h=1.5, d=2.2);
  translate([10,pcb_depth-2*pcb_radius-10,0]) cylinder(h=1.5, d=2.2);
  translate([pcb_width-2*pcb_radius-10,pcb_depth-2*pcb_radius-10,0]) cylinder(h=1.5, d=2.2);
}
